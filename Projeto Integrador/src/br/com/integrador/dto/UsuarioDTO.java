package br.com.integrador.dto;

import java.util.List;

public class UsuarioDTO {
	
	private String nome;
	private int matricula;
	private String email;
	private List<ContatoDTO> telefone;
	private List<PerfilDTO> perfil;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getMatricula() {
		return matricula;
	}
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<ContatoDTO> getTelefone() {
		return telefone;
	}
	public void setTelefone(List<ContatoDTO> telefone) {
		this.telefone = telefone;
	}
	public List<PerfilDTO> getPerfil() {
		return perfil;
	}
	public void setPerfil(List<PerfilDTO> perfil) {
		this.perfil = perfil;
	}

}
