package br.com.integrador.entity;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.integrador.dao.DB;

/**
 * Servlet implementation class Usuario
 */
@WebServlet("/Usuario")
public class Usuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Usuario() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		DB db = new DB();
		
		try {
			
			
			
			Connection con = db.getConnection(); 
			
			String script = "SELECT * FROM usuario";
			
			out.println("<h1>Cadastro de Usuario</h1>");
			
			Statement stm = con.createStatement();
			
			ResultSet rs = stm.executeQuery(script);
			
			while (rs.next()){
				
				out.println(rs.getString("nome"));
			}
			
			
		} catch (ClassNotFoundException e) {
			out.println("Erro Arquivo: " + e.getMessage());
		} catch (SQLException e) {
			out.println("Erro Driver: " + e.getMessage());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
