<%@page import="br.com.integrador.model.Aluno"%>
<%@page import="java.util.List"%>
<%@page import="br.com.integrador.dao.AlunoDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<body>	
<ul>
<%
	AlunoDao dao = new AlunoDao();
	List<Aluno> list = dao.getAll();
	
	for( Aluno aluno : list  ){
		%>
		<li> 
			<%= %>, 
			<%= aluno.getCurso() %>, 
			<%= aluno.getPeriodo() %> 
		</li>
		<%
	}
%>
</ul>
</body>

</html>