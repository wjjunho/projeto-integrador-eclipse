<%@page import="br.com.integrador.model.Aluno"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<%
	String status = (String)request.getAttribute("status");
	if( status != null){
		%>
		<p><%= status %></p>
		<%
	}
	%>
	
	<form action="aluno?action=adicionar" method="post">
	
		<label for="nome">Nome:</label>
		<input type="text" name="nome" id="nome" value="">
		<br>
		
		<label for="curso">Curso:</label>
		<input type="text" name="curso" id="curso" value="">
		<br>
		
		<label for="periodo">Periodo:</label>
		<input type="text" name="periodo" id="periodo" value="">
		<br>
		
		<input type="submit" name="submit" class="submit" value="Enviar">
		
	</form>
	
	<a href="aluno?action=listar"> voltar </a>

</body>
</html>