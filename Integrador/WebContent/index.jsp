<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

	<head> 
		<title>Projeto Integrador Alfa</title>
		<meta charset="utf-8">
		
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/estilo.css">
		
	</head>

	<body>
		
		<div id="topo">
		
			<%@ include file="fragments/topo.jsp" %>
		
		</div>
		
		<div id="meio">
		
			<div id="menu">
			
				<%@ include file="fragments/menu.jsp" %>
			
			</div>
			
			<div id="conteudo">
			
				<h1>Conte�do principal</h1>
				
				<div id="texto_principal">
					
						TEXTO INICIAL
					
				</div>
			
			
			</div>
			
		</div>
		
		<div id="rodape">
			<%@ include file="fragments/rodape.jsp" %>
		</div>
		
	</body>

</html>