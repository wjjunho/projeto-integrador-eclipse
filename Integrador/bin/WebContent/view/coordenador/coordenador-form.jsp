<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Coodenadores</title>
			<meta charset="utf-8">
		
			<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/estilo.css">

	</head>
	<body>
		
		<div id="topo">
		
			<%@ include file="/fragments/topo.jsp" %>
			
		</div>
		
		<div id="meio">
		
			<div id="menu">
			
				<%@ include file="/fragments/menu.jsp" %>
				
			</div>
			
			<div id="conteudo">
				
				<h1>Cadastro de Coordenador</h1>
				<br/><br/>
				<div id="texto_principal">
				
					<form action="mvc?logica=AdicionaCoordenador" method="post">
							
							<label for="nome">Nome:</label>
							<input type="text" name="nome" id="nome" value=""></input><br/>
							<label for="matricula">Matricula:</label>
							<input type="text" name="matricula" id="matricula" value=""></input><br/>
							<label for="email">E-mail:</label>
							<input type="text" name="email" id="email" value=""></input><br/>
							<label for="telefone">Telefone:</label>
							<input type="text" name="telefone" id="telefone" value=""></input><br/>
				
						<input type="submit" value="Salvar"></input>
					</form>
				</div>
			
			</div>
			
		</div>
		
	</body>
</html>