<%@page import="java.util.*"%>
<%@page import="br.com.integrador.dao.UsuarioDao"%>
<%@page import="br.com.integrador.model.Usuario" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
			<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/estilo.css">
		<title>Lista de Coordenador</title>
</head>
<body>
		<div id="topo">
		
			<%@ include file="/fragments/topo.jsp" %>
			
		</div>
		
		<div id="meio">
		
			<div id="menu">
			
				<%@ include file="/fragments/menu.jsp" %>
				
			</div>
			
			<div id="conteudo">
				
				<h1>Lista de Coordenador</h1>
				<br/><br/>
				
				<div id="texto_principal">
				
					<table>
						<tr>
							<th>Nome</th>
							<th>Matricula</th>
							<th>E-mail</th>
							<th>Telefone</th>
							<th>Opera��es</th>						
						</tr>
						
					<c:forEach var="coordenador" items="${coordenador}">
						<tr>
									<th>${coordenador.nome}</th>
									<th>${coordenador.matricula}</th>
									<th>${coordenador.email}</th>
									<th>${coordenador.telefone}</th>
									<th>
										<a href="mvc?logica=EditaCoordenador&id=${coordenador.id}">[Editar]</a>
										<a href="mvc?logica=RemoveCoordenador&id=${coordenador.id}">[Apaga]</a>
									</th>
						</tr>
					</c:forEach>
						
					</table>
					<form action="mvc?logica=CadastraCoordenador" method="post">
					
						<input type="submit" value="Novo Coordenador" align="left"/>
						
					</form>
					
				</div>
				
				
			</div>
			
			
		</div>
		
	

	</body>
</html>