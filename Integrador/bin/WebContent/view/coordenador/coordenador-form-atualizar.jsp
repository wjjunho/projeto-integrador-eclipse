<%@page import="com.mysql.fabric.xmlrpc.base.Param"%>
<%@page import="java.util.List"%>
<%@page import="br.com.integrador.model.Usuario" %>
<%@page import="br.com.integrador.dao.UsuarioDao" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head> 
		<title>Cadastro de Coordenador</title>
		<meta charset="utf-8">
		
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/estilo.css">
		
	</head>

	<body>
		
		<div id="topo" >
		
			<%@ include file="/fragments/topo.jsp" %>
		
		</div>
		
		<div id="meio">
		
			<div id="menu">
			
				<%@ include file="/fragments/menu.jsp" %>
			
			</div>
			
			<div id="conteudo">
			
				<h1>Atualizar Cadastro</h1>
				
				<div id="texto_principal">
					
					<form action="mvc?logica=AdicionaCoordenador" method="post">
						
							<label for="nome">Nome:</label>
							<input type="text" name="nome" id="nome" value="${coordenador.nome}"><br/>
							<label for="matricula">Matricula:</label>
							<input type="text" name="matricula" id="matricula" value="${coordenador.matricula}"><br/>
							<label for="email">E-mail:</label>
							<input type="text" name="email" id="email" value="${coordenador.email}"><br/>
							<label for="telefone">Telefone:</label>
							<input type="text" name="telefone" id="telefone" value="${coordenador.telefone}"><br/>
							<input type="hidden" name="id" id="id" value="${coordenador.id}"><br/>
				
							<input type="submit" value="Salvar"></input>
					
					</form>
					
				</div>
			
			
			</div>
			
		</div>
		
		<div id="rodape">
			<%@ include file="/fragments/rodape.jsp" %>
		</div>
		
	</body>

</html>