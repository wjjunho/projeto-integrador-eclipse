<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head> 
		<title>Tutoria</title>
		<meta charset="utf-8">
		
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/estilo.css">
		
	</head>

	<body>
		
		<div id="topo" >
		
			<%@ include file="/fragments/topo.jsp" %>
		
		</div>
		
		<div id="meio">
		
			<div id="menu">
			
				<%@ include file="/fragments/menu.jsp" %>
			
			</div>
			
			<div id="conteudo">
			
				<h1>Lista de Livros</h1>
				
				<div id="texto_principal">
					<table>
						<tr>
							<th>ISBN</th>
							<th>Nome</th>
							<th>Pre�o(R$)</th>
							<th>Editora</th>
							<th>Opera��es</th>
						</tr>
						
						</table>
						
						<a href="livro?action=adicionar">Adicionar Livro</a>
						
				</div>
			
			
			</div>
			
		</div>
		
		<div id="rodape">
			<%@ include file="/fragments/rodape.jsp" %>
		</div>
		
	</body>

</html>