<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

	<head> 
		<title>Linguagem de Programa��o III</title>
		<meta charset="utf-8">
		
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/estilo.css">
		
	</head>

	<body>
		
		<div id="topo" >
		
			<%@ include file="/fragments/topo.jsp" %>
		
		</div>
		
		<div id="meio">
		
			<div id="menu">
			
				<%@ include file="/fragments/menu.jsp" %>
			
			</div>
			
			<div id="conteudo">
			
				<h1>Lista de Livros</h1>
				
				<div id="texto_principal">
					
					<%
						String status = (String)request.getAttribute("status");
						if( status != null){
							%>
							<p> <%= status %></p>
							<%
						}
					
					%>
					
					<form action="livro?action=adicionar" method="POST">
					
						<label for="isbn">ISBN:</label>
						<input type="number" name="isbn" id="isbn" value="">
						<br>
						
						<label for="nome">Nome:</label>
						<input type="text" name="nome" id="nome" value="">
						<br>
						
						<label for="preco">Pre�o:</label>
						<input type="number" name="preco" id="preco" value="">
						<br>
						
						<label for="editora">Editora:</label>
						<input type="text" name="editora" id="editora" value="">
						<br>
						
						<input type="submit" value="Salvar" />
					
					</form>
					
				</div>
			
			
			</div>
			
		</div>
		
		<div id="rodape">
			<%@ include file="/fragments/rodape.jsp" %>
		</div>
		
	</body>

</html>