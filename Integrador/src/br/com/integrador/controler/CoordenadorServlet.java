package br.com.integrador.controler;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.integrador.dao.ProfessorDao;
import br.com.integrador.model.Coordenador;
import br.com.integrador.model.Professor;

/**
 * Servlet implementation class CoordenadorServlet
 */
@WebServlet("/coordenador")
public class CoordenadorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<Professor> professores = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CoordenadorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		Coordenador coordenador = new Coordenador();
		
		
		Professor professor = new Professor();
		
		professor.setNome(request.getParameter(""));
		
		coordenador.setProfessor(professor);
	
	
	}

}
