package br.com.integrador.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	private static final String driver = "com.mysql.jdbc.Driver";
	private static final String server = "localhost";
	private static final String dataBase = "db_tutoria";
	private static final String user = "root";
	private static final String pw = "root";
	
	public Connection getConnection() throws ClassNotFoundException, SQLException{
		
		Class.forName(driver);
		return DriverManager.getConnection("jdbc:mysql://" + server + "/" + dataBase + user + pw);
		

	}
	
}
