package br.com.integrador.model;

public class Curso {
	
	private int 	id;
	private String 	nome;
	private String 	periodo;
	
	public void Curso(int id, String nome, String periodo){
		this.id = id;
		this.nome = nome;
		this.periodo = periodo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public int getId() {
		return id;
	}
	

}
