package br.com.integrador.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.sql.PreparedStatement;

import br.com.integrador.model.Professor;
import br.com.integrador.resources.ConnectionFactory;

public class ProfessorDao implements IDAO{
	
	private Connection con;
	
	public ProfessorDao() throws ClassNotFoundException, SQLException {
		
		con = new ConnectionFactory().getConnection();
		
	}

	@Override
	public void salve() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Professor> getAll() {
		
		List<Professor> professores = null;
		String sql = "SELECT * FROM PROFESSOR";
		
		try {
			
			PreparedStatement prst = con.prepareStatement(sql);
			ResultSet rs = prst.executeQuery();
			
			while (rs.next()) {
				
				Professor professor = new Professor();
				
				professor.setNome(rs.getString("nome"));
				professor.setEmail(rs.getString("email"));
				professor.setMatricula(rs.getString("matricula"));
				
				professores.add(professor);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return professores;
	}

	@Override
	public Professor getById(int i) {
		// TODO Auto-generated method stub
		return null;
	}

}
