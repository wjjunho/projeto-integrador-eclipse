package br.com.integrador.dao;

import java.util.List;

public interface IDAO {

	public void salve();
	public void delete();
	public void update();
	public <T> List<T> getAll();
	public <T> T getById(int i);	
	
}
